## Installation

Put this file into your scripts folder and then either go to your script editor in mirc and go to remotes, then File > Load > Auto_Quote_Colorer.mrc, or open the script and copy the contents, pasting them into your remotes. It is recommended to put this script in its own file, for separation of concerns, and easier debugging.

## Usage

Go to the Aliases tab and alter your aliases as such:

How they look before the change(or similar):

`/aliasname /say <characterName> $1-`

Example:

`/ch /say <~Chris> $1-`

What they are changed to:

`/aliasname /say $chrcol(<charactername>,actionColorNumber,SpeakingColorNumber,$1-)`

If you want either of the two colors to not be colors/colorless, then simply replace the number with the control+k symbol.

`/ch /say $chrcol(<~Chris>,10,14,$1-)`

## Advanced Usage

Due to how mirc handles identifiers, a slight work-around must be made to use colored backgrounds for the alias:

`/ch $chrcol(<~Chris Wales>,$+(10,$chr(44),7),$+(5,$chr(44),4),$1-)`

or

`/ch $chrcol(<~Chris Wales>,10 $+ $chr(44) $+ 7,5 $+ $chr(44) $+ 4,$1-)`

### Parameters

`/A /say $chrcol(B,C,D,E{text}F,$1-).G`

- A = Command to use
- B = Character name
- C = Preferred action color number (no control+k symbol unless colorless)
- D = Preferred Quotation color number (no control+k symbol unless colorless)
- E = (optional) Opening marker, such as ".oO("
- F = (optional) Closing marker, such as ")"
- G = (optional) formatter name, must be prefixed with a period(`.`) after the closing `)`

The E/F parameter may be repeated as necessary to add additional markers, just as long as `$1-`(or similar) is the last parameter in the list.

Color(with the control+k symbol intact) E, F, and {text} to match color scheme desired

Example:

`/ch $chrcol(<~Some Name>,10,14,.oO({text}),$1-)`

The additional snippets may contain normally color-coded text, as long as {text} is present between the start and end bits. Each additional snippet is separated by a comma. Example:

`/ch $chrcol(<~Some Name>,10,14,.oO({text}),~{text}~,$1-)`

The `{text}` portion is a marker to indicate where to split; do not actually change the '{text}' bit in the `{text}` snippet.

**Note:**
If either of the color numbers is a single digit, you may pad it, by adding a zero **in front of it.** If it were the color of 3, it would become 03. If it were 5, it would become 05, and so on. This isn't required, but it is recommended.

### Formatters

New in 1.8.x, the ability to add formatters to specific portions of the text. To use this function, simply append `.formatterName` to the end of the alias:

Example:

`/ch $chrcol(<~Some Name>,10,14,.oO({text}),$1-).fancytext`

Note that an alias with the name **fancytext**(in this example), or the name of the formatter given, must exist for formatting to take place.

The alias is given two pieces of data; the first(`$1`) is the text passed to the formatter, while the second(`$prop`) will be either **speech** or **pairs**. The alias in question is expected to return the formatted text. **speech** refers to any quoted text, while **pairs** refers to any text within symbol pairs.

Example of the fancytext formatter:

```mirc
alias fancytext {
  if ($prop == speech) {
    var %lower = $replacecs($1-,a,𝖆,b,𝖇,c,𝖈,d,𝖉,e,𝖊,f,𝖋,g,𝖌,h,𝖍,i,𝖎,j,𝖏,k,𝖐,l,𝖑,m,𝖒,n,𝖓,o,𝖔,p,𝖕,q,𝖖,r,𝖗,s,𝖘,t,𝖙,u,𝖚,v,𝖛,w,𝖜,x,𝖝,y,𝖞,z,𝖟)
    return $replacecs(%lower,A,𝕬,B,𝕭,C,𝕮,D,𝕯,E,𝕰,F,𝕱,G,𝕲,H,𝕳,I,𝕴,J,𝕵,K,𝕶,L,𝕷,M,𝕸,N,𝕹,O,𝕺,P,𝕻,Q,𝕼,R,𝕽,S,𝕾,T,𝕿,U,𝖀,V,𝖁,X,𝖂,X,𝖃,Y,𝖄,Z,𝖅)
  }
  return $1-
}
```

The formatter alias must not be local, or if it is local,it must be in the same file as the alias that is using it.

## Note, only one version of the script can be active at a time; if upgrading, replace your current version with the new one.
