;Version: 1.8.1
;Author: Daedalus

alias -l varrete {
  return $regsubex($1,/([.()\[\]])/g,\\1)
}

alias chrcol {
  var %itminc = 1
  var %chrchr = "“”
  var %chrali = $1
  var %chracc = $chr(3) $+ $iif($len($2) == 1,0,$null) $+ $2
  var %chrspc = $chr(3) $+ $iif($len($3) == 1,0,$null) $+ $3
  var %chrpst = $eval($ $+ $0 $+ -,2)
  if ($prop != $null) {
    var %formatter = $prop
  }
  while (%itminc <= $calc($0 - 1)) {
    if (%itminc == 1) {
      if ($regex(%chrpst, /(^\s|^)[ $+ %chrchr $+ ]/i) == 1) {
        var %chrqm1 = $regsubex(%chrpst, /(^\s|^)([ $+ %chrchr $+ ])/iu,\1 $+ %chrspc $+ \2)
        if ($regex(%chrqm1, /([^ $+ %chrchr $+ ][ $+ %chrchr $+ ])(\s|$)/iu) == 1) {
          var %chrqm2 = $regsubex(%chrqm1, /([^ $+ %chrchr $+ ][ $+ %chrchr $+ ])(\s|$)/iu, \1 $+ $chr(3) $+ %chracc $+ $chr(32))
        }
        else {
          var %chrqm2 = %chrpst
        }
      }
      else {
        var %chrqm2 = %chrpst
      }
      while ($regex(%chrqm2, /\s[ $+ %chrchr $+ ]/iu) == 1) {
        var %chrqm2 = $regsubex(%chrqm2, /\s([ $+ %chrchr $+ ])/iu, $chr(32) $+ %chrspc $+ \1)
        while ($regex(%chrqm2,/([^ $+ %chrchr $+ ][ $+ %chrchr $+ ])(\s|$)/iu) == 1) {
          var %chrqm2 = $regsubex(%chrqm2, /([^ $+ %chrchr $+ ][ $+ %chrchr $+ ])(\s|$)/iu, \1 $+ $chr(3) $+ %chracc $+ $chr(32))
        }
      }
      if (%formatter) {
        var %chrqm2 = $regsubex(%chrqm2,/([ $+ %chrchr $+ ])([^ $+ %chrchr $+ ]*)([ $+ %chrchr $+ ])/ig,\1 $+ $eval($ $+ %formatter $+ (\2).speech,2) $+ \3)
      }
      var %chrpst = %chrqm2
      inc %itminc 3
    }
    else {
      var %chrstr = $eval($ $+ %itminc,2)
      if ($regex(%chrstr,/^(.+?)\{text\}(.+?)$/i) == 1) {
        var %chrchrs = $regml(1)
        var %chrchre = $regml(2)
        var %chrchrs_reg = $strip($varrete(%chrchrs))
        var %chrchre_reg = $strip($varrete(%chrchre))
        var %symbolboundrys = (^|[\s]|[\002\035\037]|[\003][0-9,]*)
        var %symbolboundrye = ([\s]?|[\002\035\037]?|[\003]?[0-9,]*)
        var %wordchrclass = [\/\s\w\.\,\'\-\+\=\:\;\!\?\*\&\^\%\$\#\@\002\035\037]
        var %repreg = / $+ %symbolboundrys $+ %chrchrs_reg $+ ( $+ %wordchrclass $+ +?) $+ %chrchre_reg $+ %symbolboundrye $+ /igu
        if ($regex(hasQuote, %chrstr, / $+ $+($chr(91),%chrchr,$chr(93)) $+ /u) == 1) {
          var %start = $regsubex(%chrchrs_reg, /[ $+ %chrchr $+ ]/, $null)
          var %reg_space = / $+ $+(%start,$chr(32),%chrspc,$chr(91),%chrchr,$chr(93)) $+ /iu
          if ($regex(coloredQuoteSpace, %chrqm2, %reg_space) == 1) {
            var %chrchrs_reg = $regsubex(%chrchrs_reg, / $+ $+($chr(91),%chrchr,$chr(93)) $+ /, $+(%chrspc,$chr(91),%chrchr,$chr(93)))
            var %chrchre_reg = $regsubex(%chrchre_reg, / $+ $+($chr(91),%chrchr,$chr(93)) $+ /, $+($chr(91),%chrchr,$chr(93),$chr(3),%chracc))
            var %repreg = / $+ %symbolboundrys $+ %chrchrs_reg $+ ( $+ %wordchrclass $+ +?) $+ %chrchre_reg $+ %symbolboundrye $+ /igu
          }
        }
        if (%formatter) {
          var %chrqm2 = $regsubex(%chrqm2, %repreg, \1 $+ %chrchrs $+ $eval($ $+ %formatter $+ (\2).pairs,2) $+ %chrchre $+ \3 $+ %chracc)
          ; Run it twice to make sure any left over are captured
          var %chrqm2 = $regsubex(%chrqm2, %repreg, \1 $+ %chrchrs $+ $eval($ $+ %formatter $+ (\2).pairs,2) $+ %chrchre $+ \3 $+ %chracc)
        }
        else {
          var %chrqm2 = $regsubex(%chrqm2, %repreg, \1 $+ %chrchrs $+ \2 $+ %chrchre $+ \3 $+ %chracc)
          ; Run it twice to make sure any left over are captured
          var %chrqm2 = $regsubex(%chrqm2, %repreg, \1 $+ %chrchrs $+ \2 $+ %chrchre $+ \3 $+ %chracc)
        }
      }
      inc %itminc 1
    }
  }
  return %chrali %chracc $+ %chrqm2
}
