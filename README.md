Copyright (c) 2019 daedalus11069

Auto-colors quote and symbol pairs.  For installation and usage, please see the [wiki](https://bitbucket.org/daedalus11069/auto-alias-quote-colorer/wiki/Home).

[Change Log](https://bitbucket.org/daedalus11069/auto-alias-quote-colorer/src/master/CHANGELOG.md)

There are no plans to license this code; do not distribute without permission.
